#include <Wire.h>
#include <GridEye.h>
#include "BluetoothSerial.h"

GridEye gnd;
GridEye vdd = GridEye(GridEye_DeviceAddress_1);
BluetoothSerial SerialBT;


// ピクセル温度データ保存用
int pixel_gnd[64];
int pixel_vdd[64];
float temperature_gnd;
float temperature_vdd;

void setup(void)
{ 
  pinMode(26,OUTPUT);
  // I2Cバスに参加
  Wire.begin();

  // Bluetoothペアリングの開始
  SerialBT.begin("ESP32");

  // シリアルポート初期化
  Serial.begin(115200);
  //SerialBT.begin(9600);

  while (!Serial) {
    ; // シリアルポート接続待ち
  }

  // サーミスタ温度読み出し
  int temp_gnd = gnd.thermistorTemp();
  int temp_vdd = vdd.thermistorTemp();
//  Serial.print(F("Thermistor Temp: "));
//  Serial.println(temp * 0.065); // 1単位 = 0.065度
}

void loop(void)
{
  // ピクセル温度データ読み出し
  
  gnd.pixelOut(pixel_gnd);
  digitalWrite(26,HIGH);
  vdd.pixelOut(pixel_vdd);
  digitalWrite(26,LOW);
  SerialBT.print("[\n");
  
  Serial.print("[\n");
    
  for (int i = 0; i < 64; i++) {
    if (i && ((i % 8) == 0)) {
      SerialBT.println();
      Serial.println();
    }
    
    if(pixel_gnd[i] > 0x200)
    {
        temperature_gnd = (-pixel_gnd[i] +  0xfff) * -0.25;
    }else
    {
        temperature_gnd = pixel_gnd[i] * 0.25;
    }
    

    if(pixel_vdd[i] > 0x200)
    {
        temperature_vdd = (-pixel_vdd[i] +  0xfff) * -0.25;
    }else
    {
        temperature_vdd = pixel_vdd[i] * 0.25;
    }
    
    SerialBT.print(temperature_gnd);
    SerialBT.print("|");
    SerialBT.print(temperature_vdd);
    SerialBT.print(",");

    Serial.print(temperature_gnd);
    Serial.print("|");
    Serial.print(temperature_vdd);
    Serial.print(",");
  }
  
  SerialBT.println("\n]");
  
  Serial.println("\n]");
  
  // 10ms待つ
  delay(10);
  
}
