# シリアル通信ライブラリ追加
add_library('serial')

# 最低・最大温度
min_temp = 20 / 0.25
max_temp = 38 / 0.25

def setup():
# ウィンドウタイトルと画面サイズ設定
    frame.setTitle("GridEye View")
    size(384 * 2, 384)

    # シリアルポート初期化
    print Serial.list()
    portIndex = 4  # 環境に合わせて変更してください
    print "Connecting to", Serial.list()[portIndex]
    global myPort
    myPort = Serial(this, Serial.list()[portIndex], 115200)

    # ピクセル温度データ初期化
    global pixel_buffer
    pixel_buffer = [0 for i in range(64)]

    # 8x8イメージ作成
    global pg
    pg = createGraphics(8, 8)

    # グラデーションテーブル作成
    global gradient
    gradient = []
    last_c = color(6, 3, 25)
    base_c = [color(51, 24, 131),
            color(163, 38, 135),
            color(210, 38, 41),
            color(239, 121, 37),
            color(250, 216, 54),
            color(254, 247, 210)]
    for c in base_c:
        for i in range(15):
            # 中間色を追加
            inter = lerpColor(last_c, c, .0625 * (i + 1))
            gradient.append(inter)
        last_c = c
    # print len(gradient)
    # print gradient

def draw():
    global pixel_buffer
    # ピクセルデータ受信
    while myPort.available() >= 130:
        # ヘッダチェック1
        if myPort.read() == 0x55:
            # ヘッダチェック2
            if myPort.read() == 0xaa:
                # 128バイト分読み出し
                for i in range(64):
                    # 1ピクセル2バイトの整数型で下位バイト、上位バイトの順で送られてくる
                    lo = myPort.read()
                    hi = myPort.read()
                    temp = (hi << 8) | lo
                    pixel_buffer[i] = temp
                print pixel_buffer
                break
    # 8x8イメージ更新
    pg.beginDraw()
    for y in range(pg.height):
        for x in range(pg.width):
            # 温度を色に変換
            c = temp2Color(pixel_buffer[((8 - y - 1) * 8) + (8 - x - 1)])
            # ピクセル設定
            pg.set(x, y, c)
    pg.endDraw()

    # ウィンドウの左側に転送
    half_width = width / 2
    image(pg, 0, 0, half_width, height)

    # ウィンドウの右側にグリッド状に描画
    rw = half_width / 8
    rh = height / 8
    padding = 4
    fill(15)
    noStroke()
    rect(half_width, 0, half_width, height)
    for y in range(pg.height):
        for x in range(pg.width):
            fill(pg.get(x, y))
            rect(half_width + (rw * x) + padding, (rh * y) + padding,
                rw - (padding * 2), rh - (padding * 2), 5)

def temp2Color(temp):
    i = map(constrain(temp, min_temp, max_temp),
            min_temp, max_temp, 0, len(gradient) - 1)
    return gradient[int(i)]
